Fabric is a computer music system written with the Python v2.x standard library.
It is intended to be used for offline/non-realtime computer music composition. 

It aims to be highly portable, hackable, understandable and powerful. 

A roadmap and some documentation can be found on [the fabric website](http://fabric-dsp.com)
